//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit

final class ___FILEBASENAME___ {

    // MARK: - Methods
    func configureModuleForViewInput<UIViewController>(_ viewInput: UIViewController) {
        if let viewController = viewInput as? ___VARIABLE_moduleName___ViewController {
            configure(viewController)
        }
    }

    private func configure(_ viewController: ___VARIABLE_moduleName___ViewController) {
        let router = ___VARIABLE_moduleName___Router()
        router.transitionHandler = viewController

        let presenter = ___VARIABLE_moduleName___Presenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ___VARIABLE_moduleName___Interactor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.moduleInput = presenter
    }
}

