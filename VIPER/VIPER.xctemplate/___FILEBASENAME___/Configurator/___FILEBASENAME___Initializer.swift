//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit

final class ___FILEBASENAME___: NSObject {

    // TODO: Connect with object on storyboard
    @IBOutlet weak var viewController: ___VARIABLE_moduleName___ViewController!

    override func awakeFromNib() {
      let configurator = ___VARIABLE_moduleName___ModuleConfigurator()
      configurator.configureModuleForViewInput(viewController)
    }
}
