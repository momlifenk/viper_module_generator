//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation
import UIKit

final class ___FILEBASENAME___: UIViewController, ViperModuleTransitionHandler, ViperModuleConfigurationHolder {
    
    // MARK: - Properties
    var output: ___VARIABLE_moduleName___ViewOutput!
    var moduleInput: ___VARIABLE_moduleName___ModuleInput!
    
    // MARK: - @IBOutlets
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    // MARK: @IBActions
    
}

extension ___FILEBASENAME___: ___VARIABLE_moduleName___ViewInput {
    func setupInitialState() {}
}

extension ___FILEBASENAME___: PresentableModule {
    static func storyboard() -> MLStoryboard {
        //TODO: change it
        return .___VARIABLE_moduleName___
    }
}

