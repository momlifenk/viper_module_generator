//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation

final class ___FILEBASENAME___ {

    // MARK: Properties
    weak var view: ___VARIABLE_moduleName___ViewInput!
    var interactor: ___VARIABLE_moduleName___InteractorInput!
    var router: ___VARIABLE_moduleName___RouterInput!
    weak var moduleOutput: ViperModuleOutputType?
}

extension ___FILEBASENAME___: ___VARIABLE_moduleName___ModuleInput {
    func setInitialData() {
        // TODO: add functinality
    }
}

extension ___FILEBASENAME___: ___VARIABLE_moduleName___InteractorOutput {
}

extension ___FILEBASENAME___: ___VARIABLE_moduleName___ViewOutput {
    func viewIsReady() {
        view.setupInitialState()
    }
}
