//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit

final class ___FILEBASENAME___: ___VARIABLE_moduleName___RouterInput {
    weak var transitionHandler: ViperModuleTransitionHandler!
    
    func dismiss() {
        transitionHandler.popModule()
    }
}


