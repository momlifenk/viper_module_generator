//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation

final class ___FILEBASENAME___ {
    // MARK: Properties
    weak var output: ___VARIABLE_moduleName___InteractorOutput!
}

extension ___FILEBASENAME___: ___VARIABLE_moduleName___InteractorInput  {
    // TODO: add methods
}
