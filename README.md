# ViperTemplate
Template to generate VIPER classes on Xcode.

## Installation
- Download VIPER Template or clone the project
- Copy the `VIPER` folder to `~/Library/Developer/Xcode/Templates/File Templates/`(create these folders if they don't exist) or create a symbolic link to that folder.

## Using the template
- Start Xcode
- Create a new file (`File > New > File` or `⌘N`)
- Choose `VIPER` 

## Created Files
- FolderWithModuleName
	- `Configurator`
	- `Interactor`
	- `Presenter`
	- `Router` 
	- `View`
